package com.example.chaitsereimp.database.list

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "list_item")
data class ListItem(
    var name:String,
    var itemId:String
){
    @PrimaryKey(autoGenerate = true)
    var id:Int? = null
}
