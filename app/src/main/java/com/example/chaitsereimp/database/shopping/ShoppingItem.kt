package com.example.chaitsereimp.database.shopping

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shopping_item")
data class ShoppingItem(
    var name:String,
    var amount:String
){
    @PrimaryKey(autoGenerate = true)
    var id:Int? = null
}