package com.example.chaitsereimp.database.shopping

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(
    entities = [ShoppingItem::class],
    version = 1
)
abstract class ShoppingItemDb:RoomDatabase() {
    abstract fun getsShoppingDao():ShoppingDao
}