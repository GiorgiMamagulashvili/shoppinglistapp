package com.example.chaitsereimp.database.list

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [ListItem::class],
    version = 1
)
abstract class ListItemDB :RoomDatabase(){
    abstract fun getListDao(): ListItemDao
}