package com.example.chaitsereimp.database.list

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ListItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItem(item: ListItem)

    @Delete
    suspend fun deleteItem(item: ListItem)

    @Query("SELECT * FROM list_item")
    fun readAllListItem(): LiveData<List<ListItem>>

}