package com.example.chaitsereimp.repository

import com.example.chaitsereimp.database.list.ListItem
import com.example.chaitsereimp.database.list.ListItemDao
import javax.inject.Inject

class ListRepository @Inject constructor(
    val listDao: ListItemDao
) {

    suspend fun insertItem(item: ListItem) = listDao.insertItem(item)

    suspend fun deleteItem(item: ListItem) = listDao.deleteItem(item)

    fun readAllListData() = listDao.readAllListItem()
}