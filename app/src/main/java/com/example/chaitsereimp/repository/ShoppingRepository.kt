package com.example.chaitsereimp.repository

import com.example.chaitsereimp.database.shopping.ShoppingDao
import com.example.chaitsereimp.database.shopping.ShoppingItem
import javax.inject.Inject

class ShoppingRepository @Inject constructor(
    val shoppingDao: ShoppingDao
) {

    suspend fun insertItem(item: ShoppingItem) = shoppingDao.insertItem(item)

    suspend fun deleteItem(item: ShoppingItem) = shoppingDao.deleteItem(item)

    fun readAllShoppingItemData() = shoppingDao.readAllItemData()
}