package com.example.chaitsereimp.ui

import com.example.chaitsereimp.database.list.ListItem

interface AddListDialogListener {

    fun onListAddButtonClickListener(item:ListItem)
}