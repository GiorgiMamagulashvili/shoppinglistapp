package com.example.chaitsereimp.ui.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.chaitsereimp.database.list.ListItem
import com.example.chaitsereimp.databinding.FragmentAddListItemBinding
import com.example.chaitsereimp.ui.AddListDialogListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddListItemFragment(
    context: Context,
    val listener: AddListDialogListener
) : DialogFragment() {

    lateinit var binding: FragmentAddListItemBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        binding = FragmentAddListItemBinding.inflate(layoutInflater, container, false)
        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE);
        }

        binding.cancelButtonList.setOnClickListener {
            dismiss()
        }
        binding.listItemAddButton.setOnClickListener {
            val name = binding.listItemNameEt.text.toString()
            val id = binding.listItemIdEt.text.toString()

            if (name.isNotEmpty() && id.isNotEmpty()) {
                val listItem = ListItem(name, id)
                listener.onListAddButtonClickListener(listItem)
                dismiss()
            } else {
                Toast.makeText(requireContext(), "Please Fill all Fields!!", Toast.LENGTH_SHORT)
                    .show()
            }

        }


        return binding.root
    }

}