package com.example.chaitsereimp.ui.fragment

import android.graphics.drawable.AnimationDrawable
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chaitsereimp.R
import com.example.chaitsereimp.adapter.ListItemAdapter
import com.example.chaitsereimp.database.list.ListItem
import com.example.chaitsereimp.databinding.FragmentListBinding
import com.example.chaitsereimp.ui.AddListDialogListener
import com.example.chaitsereimp.ui.viewmodel.ListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListFragment : Fragment(R.layout.fragment_list) {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val listViewModel: ListViewModel by viewModels()
    private val mAdapter: ListItemAdapter by lazy { ListItemAdapter(listViewModel) }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentListBinding.bind(view)
        setUpRecycle()
        backGroundAnimation()


        listViewModel.readListData.observe(viewLifecycleOwner, Observer {
            mAdapter.setData(it)
        })

        binding.fab.setOnClickListener {
            binding.fab.animate().apply {
                rotationBy(360f)
                duration = 100
            }.start()

            AddListItemFragment(
                requireContext(),
                object :AddListDialogListener{
                    override fun onListAddButtonClickListener(item: ListItem) {
                        listViewModel.insertData(item)
                    }

                }
            ).show(childFragmentManager,"DialogAddListITem")
        }

        val animation = TransitionInflater.from(requireContext()).inflateTransition(
            android.R.transition.move
        )
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation

    }

    private fun setUpRecycle() {
        binding.recycleView.layoutManager = LinearLayoutManager(requireContext())
        binding.recycleView.adapter = mAdapter
    }

    private fun backGroundAnimation() {
        val animationDrawable: AnimationDrawable =
            binding.listContainer.background as AnimationDrawable
        animationDrawable.apply {
            setEnterFadeDuration(1000)
            setExitFadeDuration(3000)
            start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}