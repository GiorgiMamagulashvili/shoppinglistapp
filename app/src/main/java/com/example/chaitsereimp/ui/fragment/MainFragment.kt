package com.example.chaitsereimp.ui.fragment

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.chaitsereimp.R
import com.example.chaitsereimp.databinding.FragmentMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding get() = _binding!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        _binding = FragmentMainBinding.bind(view)

        backGroundAnimation()

        binding.ivShoppingList.setOnClickListener {
            val extras = FragmentNavigatorExtras(binding.ivShoppingList to "image_big")
            findNavController().navigate(
                R.id.action_mainFragment_to_listFragment,
                null,
                null,
                extras
            )
        }
        binding.ivShoppingBucket.setOnClickListener {
            val extras = FragmentNavigatorExtras(binding.ivShoppingBucket to "container_first")
            findNavController().navigate(
                R.id.action_mainFragment_to_shoppingFragment,
                null,
                null,
                extras
            )
        }

    }

    private fun backGroundAnimation() {
        val animationDrawable: AnimationDrawable = binding.layoutC.background as AnimationDrawable
        animationDrawable.apply {
            setEnterFadeDuration(1000)
            setExitFadeDuration(3000)
            start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}