package com.example.chaitsereimp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chaitsereimp.database.list.ListItem
import com.example.chaitsereimp.repository.ListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class ListViewModel @Inject constructor(
    private val repo:ListRepository
) : ViewModel(){

    fun insertData(Item:ListItem){
        viewModelScope.launch (Dispatchers.IO){
            repo.insertItem(Item)
        }
    }

    fun deleteData(Item: ListItem){
        viewModelScope.launch (Dispatchers.IO){
            repo.deleteItem(Item)
        }
    }
    val readListData = repo.readAllListData()
}