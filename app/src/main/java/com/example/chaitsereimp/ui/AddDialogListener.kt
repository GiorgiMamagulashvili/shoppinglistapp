package com.example.chaitsereimp.ui

import com.example.chaitsereimp.database.shopping.ShoppingItem

interface AddDialogListener {
    fun onAddButtonListener(item:ShoppingItem)
}