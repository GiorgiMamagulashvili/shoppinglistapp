package com.example.chaitsereimp.ui.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.chaitsereimp.R
import com.example.chaitsereimp.adapter.ShoppingItemRecycleAdapter
import com.example.chaitsereimp.database.shopping.ShoppingItem
import com.example.chaitsereimp.databinding.FragmentAddShoppingItemDialogBinding
import com.example.chaitsereimp.ui.AddDialogListener
import com.example.chaitsereimp.ui.viewmodel.ShoppingItemViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp

@AndroidEntryPoint
class AddShoppingItemDialogFragment(context: Context, val listener:AddDialogListener) : DialogFragment() {

    private val shoppingViewModel: ShoppingItemViewModel by viewModels()
    private val myAdapter: ShoppingItemRecycleAdapter by lazy { ShoppingItemRecycleAdapter(shoppingViewModel) }
    lateinit var binding:FragmentAddShoppingItemDialogBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddShoppingItemDialogBinding.inflate(layoutInflater,container,false)
        binding.shoppItemAddButton.setOnClickListener {
            val name = binding.etName.text.toString()
            val amount =binding.etAmount.text.toString()
            if (name.isNotEmpty() && amount.isNotEmpty()){
                val item = ShoppingItem(name,amount)
                listener.onAddButtonListener(item)
                dismiss()
            }else{
                Toast.makeText(requireContext(), "Please fill all fields!!", Toast.LENGTH_SHORT).show()
            }


        }
        binding.cancelButtonShopping.setOnClickListener {
            dismiss()
        }

        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE);
        }

        return binding.root
    }
    private fun addData(){
        val name = binding.etName.text.toString()
        val amount = binding.etAmount.text.toString()

        if (name.isNotEmpty() && amount.isNotEmpty()){
            val item = ShoppingItem(name,amount)
            shoppingViewModel.insertData(item)
            Toast.makeText(requireContext(), "GG", Toast.LENGTH_SHORT).show()

        }else{
            Toast.makeText(requireContext(), "Please fill All field!", Toast.LENGTH_SHORT).show()
        }
    }

}