package com.example.chaitsereimp.ui.fragment

import android.graphics.drawable.AnimationDrawable
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chaitsereimp.R
import com.example.chaitsereimp.adapter.ShoppingItemRecycleAdapter
import com.example.chaitsereimp.database.shopping.ShoppingItem
import com.example.chaitsereimp.databinding.FragmentShoppingBinding
import com.example.chaitsereimp.ui.AddDialogListener
import com.example.chaitsereimp.ui.viewmodel.ShoppingItemViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShoppingFragment : Fragment(R.layout.fragment_shopping) {

    private var _binding: FragmentShoppingBinding? = null
    private val binding: FragmentShoppingBinding get() = _binding!!

    private val shoppingViewModel: ShoppingItemViewModel by viewModels()
    private val myAdapter: ShoppingItemRecycleAdapter by lazy {
        ShoppingItemRecycleAdapter(
            shoppingViewModel
        )
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentShoppingBinding.bind(view)

        backGroundAnimation()
        setRecycle()
        shoppingViewModel.readData.observe(viewLifecycleOwner, Observer {
            myAdapter.setShoppingData(it)
        })

        val animation = TransitionInflater.from(requireContext()).inflateTransition(
            android.R.transition.move,
        )
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation

        binding.fab.setOnClickListener {
            AddShoppingItemDialogFragment(
                requireContext(),
                object : AddDialogListener {
                    override fun onAddButtonListener(item: ShoppingItem) {
                        shoppingViewModel.insertData(item)
                    }

                }
            ).show(childFragmentManager,"AddDialog")
        }

    }

    private fun backGroundAnimation() {
        val animationDrawable: AnimationDrawable =
            binding.shopContainer.background as AnimationDrawable
        animationDrawable.apply {
            setEnterFadeDuration(1000)
            setExitFadeDuration(3000)
            start()
        }
    }

    private fun setRecycle() {
        binding.shoppingRecycleView.adapter = myAdapter
        binding.shoppingRecycleView.layoutManager = LinearLayoutManager(context)

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}