package com.example.chaitsereimp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chaitsereimp.database.shopping.ShoppingItem
import com.example.chaitsereimp.repository.ShoppingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class ShoppingItemViewModel @Inject constructor(
    private val repo: ShoppingRepository
) : ViewModel() {

    fun insertData(item: ShoppingItem) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.insertItem(item)
        }
    }

    fun deleteData(item: ShoppingItem) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.deleteItem(item)
        }
    }

    val readData = repo.readAllShoppingItemData()
}