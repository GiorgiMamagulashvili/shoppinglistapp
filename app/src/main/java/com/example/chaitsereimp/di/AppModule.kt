package com.example.chaitsereimp.di

import android.content.Context
import androidx.room.Room
import com.example.chaitsereimp.database.list.ListItemDB
import com.example.chaitsereimp.database.shopping.ShoppingItemDb
import com.example.chaitsereimp.other.Constants.LIST_DATABASE_NAME
import com.example.chaitsereimp.other.Constants.SHOPPING_DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Provides
    fun provideShoppingDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        ShoppingItemDb::class.java,
        SHOPPING_DATABASE_NAME
    ).build()


    @Provides
    fun provideGetShoppingItemDao(db: ShoppingItemDb) = db.getsShoppingDao()

    @Provides
    fun provideListItemDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        ListItemDB::class.java,
        LIST_DATABASE_NAME
    ).build()

    @Provides
    fun provideGetListDao(db: ListItemDB) = db.getListDao()
}