package com.example.chaitsereimp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chaitsereimp.database.shopping.ShoppingItem
import com.example.chaitsereimp.databinding.ShoppingItemRowBinding
import com.example.chaitsereimp.ui.viewmodel.ShoppingItemViewModel

class ShoppingItemRecycleAdapter(
    private val mViewModel:ShoppingItemViewModel
) :
    RecyclerView.Adapter<ShoppingItemRecycleAdapter.ShoppingViewHolder>() {

    private var oldList = emptyList<ShoppingItem>()

    inner class ShoppingViewHolder(val binding: ShoppingItemRowBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingViewHolder {
        return ShoppingViewHolder(
            ShoppingItemRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ShoppingViewHolder, position: Int) {
        holder.binding.tvShoppingItemAmount.text = oldList[position].amount
        holder.binding.tvShoppingItemName.text = oldList[position].name
        holder.binding.shoppingItemDellBt.setOnClickListener {
            mViewModel.deleteData(oldList[position])
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount(): Int {
        return oldList.size
    }

    fun setShoppingData(newList: List<ShoppingItem>) {
        oldList = newList
        notifyDataSetChanged()
    }

}
