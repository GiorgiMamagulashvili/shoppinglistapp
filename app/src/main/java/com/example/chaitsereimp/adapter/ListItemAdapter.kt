package com.example.chaitsereimp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListView
import androidx.recyclerview.widget.RecyclerView
import com.example.chaitsereimp.database.list.ListItem
import com.example.chaitsereimp.databinding.IdListItemRowBinding
import com.example.chaitsereimp.ui.viewmodel.ListViewModel

class ListItemAdapter(
    val mViewModel:ListViewModel
):RecyclerView.Adapter<ListItemAdapter.MyViewHolder>() {

    private var oldList = emptyList<ListItem>()

    inner class MyViewHolder(val binding: IdListItemRowBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(IdListItemRowBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvIdName.text = oldList[position].name
        holder.binding.tvId.text = oldList[position].itemId

        holder.binding.idListDell.setOnClickListener {
            mViewModel.deleteData(oldList[position])
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount(): Int {
        return oldList.size
    }

    fun setData(newList:List<ListItem>){
        oldList = newList
        notifyDataSetChanged()
    }
}